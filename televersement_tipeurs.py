# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 15:47:25 2017

@author: Globulle

------------------------------------

Script pour convertir les images .png en .jpg et les exporter sur le serveur.

Requiert 2 fichiers "template" : config.json et index.html
(+ pass.php pour la version tipeurs)

Les fichiers images d'entrée doivent être organisés comme suit :
 - Pages
     - begin
         - image de couverture
         - eventuelles autres pages de préambule
     - chap_00
         - 01.png
         - 02.png
         - ...
     - chap_01
         - 01.png
         - 02.png
         - ...
     - ...
     - end
         - image de conclusion
         
Il est possible d'ajoindre des notes à certaines pages : celles-ci doivent être
rédigées dans un fichier de même nom que l'image afférente (ex : 02.txt).

Pour reconvertir et ré-exporter l'ensemble des images, supprimer le dossier TO_EXPORT.
Par défaut, les images déjà présentes ne sont pas retraitées.

"""

import os
import json
import time
from ftplib import FTP   
import yaml

# %%
#site_directory = "mat&met_tipeurs"
#site_path = "/manga/" + site_directory
#pageFolder = 'Pages'
#templateFolder = 'Templates'
#destFolder = 'TO_EXPORT' # A effacer pour relancer l'ensemble de l'export

def read_parameters(filename):
    param = yaml.load(open(filename))
    return param

def create_json_page_entry(page_nb):             # %% Page entry in json file
    hotspot_text = unicode('top:0.00%;left:0.00%;width:100.00%;height:100.00%;') # unicode chain
    hotspot_entry = [hotspot_text] # list
    pagetext_entry = [] #list (possibility to add the dialogs)
    if not (folder=='begin' or folder=='end'):
        scripttext_entry = unicode('Chap.%d - p.%d' % (f,page_nb))
    elif folder=='begin':
        scripttext_entry = unicode('Ouverture - p.%d' % page_nb)
    else:
        scripttext_entry = unicode('Cloture - p.%d' % page_nb)
            
    json_page_entry = {unicode('hotspots'): hotspot_entry, unicode('number'): page_nb,
                       unicode('page_text'): pagetext_entry, unicode('script_text'): scripttext_entry}
    return json_page_entry


def write_metadata(image_name,param,page_name):
    os.system('exiftool -all= -overwrite_original -q %s' % image_name) # Metadata : remove everything for privacy    
    year = time.strftime('%Y',time.localtime())
    copyright_mention = "Copyright (c) " + param['creator'] + " " + year
    cmd = 'exiftool '\
    ' -EXIF:XPTitle="' + page_name + \
    '" -IPTC:ObjectName="'+ page_name + \
    '" -XMP-dc:Title="'+ page_name + \
    '" -XMP-xmpDM:ShotName="'+ page_name + \
    '" -EXIF:XPAuthor="' + param['creator'] + \
    '" -IPTC:By-line="' + param['creator'] + \
    '" -XMP-dc:Creator="' + param['creator'] + \
    '" -XMP-pdf:Author="' + param['creator'] + \
    '" -EXIF:Artist="' + param['creator'] + \
    '" -EXIF:Copyright="' + copyright_mention + \
    '" -EXIF:OwnerName="' + param['creator'] + \
    '" -EXIF:Usercomment="' + param['licence'] + \
    '" -EXIF:XPAuthor="' + param['creator'] + \
    '" -EXIF:XPComment="' + copyright_mention + \
    '" -IPTC:By-line="' + param['creator'] + \
    '" -IPTC:Contact="' + param['url'] + \
    '" -IPTC:CopyrightNotice="' + param['licence'] + \
    '" -IPTC:Credit="' + param['creator'] + \
    '" -Photoshop:CopyrightFlag="True' + \
    '" -Photoshop:URL="' + param['url'] + \
    '" -XMP-aux:OwnerName="' + param['creator'] + \
    '" -XMP-dc:Source="' + param['creator'] + \
    '" -XMP-dc:Creator="' + param['creator'] + \
    '" -XMP-dc:Rights="' + param['licence'] + \
    '" -XMP-pdf:Author="' + param['creator'] + \
    '" -XMP-photoshop:Credit="' + param['creator'] + \
    '" -XMP-photoshop:Source="' + param['creator'] + \
    '" -XMP-xmpDM:Copyright="' + copyright_mention + \
    '" -XMP-xmpRights:Marked="True' + \
    '" -XMP-xmpRights:Owner="' + param['creator'] + \
    '" -XMP-xmpRights:UsageTerms="' + param['licence'] + \
    '" -XMP-xmpRights:WebStatement="' + param['url'] + \
    '" -overwrite_original -q ' + image_name 
    os.system(cmd)


# %% Getting parameters
param = read_parameters('parameters.yaml')
param['site_dir'] = param['site_dir_tipeurs'] # Overwrite site_dir parameter
site_path = "/" + param['site_metadir'] + "/" + param['site_dir']


# %% Listing files
lstFolders = os.listdir(param['page_folder'])
lstFolders.sort()

lstPNG = []
for folder in lstFolders:
    path = os.path.join(param['page_folder'],folder)
    files = os.listdir(path)
    pngfiles = [x for x in files if x.endswith('.png')] # only png files
    pngfiles.sort()
    lstPNG.append(pngfiles)
#    print pngfiles

destFolder_pages = os.path.join(param['dest_folder'],'pages_tipeurs')

if not os.path.isdir(param['dest_folder']): # creation of destination folders
    os.mkdir(param['dest_folder'])
if not os.path.isdir(destFolder_pages):
    os.mkdir(destFolder_pages)
    

# %% 
page_nb=0
bookmarks = [] # page numbers for the beginning of chapters
json_page_entries = []
i_note = 0 # note counter
json_endnote_entries = []
pages_to_send = []
for f,folder in enumerate(lstFolders):
    for page in lstPNG[f]:
        page_nb+=1
        new_page_name = 'page-%d.jpg' % page_nb
        dest_path = os.path.join(destFolder_pages,new_page_name)
        source_path = os.path.join(param['page_folder'],folder,page)
        if os.path.isfile(dest_path):
            print 'Fichier %s déjà existant.' % new_page_name
        else:
#            print 'convert %s  %s' % (source_path, dest_path)
            os.system('convert -resize 75%% -quality 60%% -unsharp 0.48x0.48+0.50+0.012 %s  %s' % (source_path, dest_path)) # reduce size and quality
            metadata_title = folder + "-" + new_page_name      
            write_metadata(dest_path,param,metadata_title)            
            print 'Fichier %s écrit.' % new_page_name
            pages_to_send.append(new_page_name)

        
        # %% Page entry in json file
        json_page_entry = create_json_page_entry(page_nb)
        json_page_entries.append(json_page_entry)

        
        # %% Endnote entry in json file
        note_path = source_path[:-3] + 'txt'
        if os.path.exists(note_path):
            i_note += 1
            note_file = open(note_path,'r')
            text_entry = unicode(note_file.read())
            note_file.close()
            # url_entry = # useless since note text accepts html code
            pagepanel_entry = unicode('p.%d' % page_nb)
            json_note_entry = {unicode('number'):i_note, unicode('page_panel'): page_nb,
                               unicode('text'): text_entry}
            json_endnote_entries.append(json_note_entry)
            print "Note ajoutée."
            
    if (page_nb % 2 == 1) and len(lstPNG[f])>0: #odd test Add a white page to have chapter begin on right (even) side
        page_nb+=1
        new_page_name = 'page-%d.jpg' % page_nb
        dest_path = os.path.join(destFolder_pages,new_page_name)
        source_path = os.path.join(param['page_folder'],folder,lstPNG[f][-1])
        os.system('convert %s  -fill white -colorize 100%% -resize 75%% %s' % (source_path, dest_path)) # Producing a fully white image
        print 'Fichier blanc %s écrit.' % new_page_name
        pages_to_send.append(new_page_name)
        json_page_entry = create_json_page_entry(page_nb)
        json_page_entries.append(json_page_entry)
        
    bookmarks.append(page_nb+1)

json_content = {unicode('endnotes'):json_endnote_entries, unicode('pages'):json_page_entries}

with open(param['dest_folder'] + '/pages.json', 'w') as outfile:
    json.dump(json_content, outfile)
print 'Fichier pages.json écrit.'

# %% Chapter index in index.html
html_file = open(os.path.join(templateFolder,'index.html'), 'r')
html_text = html_file.read()
html_file.close()
text = r"""<a href=""><li>Retour début</li></a>
    """
chap_nb = 0
for p in bookmarks[0:-2]:
    text = text + (r"""<a href="#%d"><li>Chapitre %d</li></a>
    """ % (p,chap_nb))
    chap_nb +=1
text = text + (r"""<a href="#%d"><li>...</li></a>""" % bookmarks[-2])

new_text = html_text.replace("<!-- LIST_OF_CHAPTERS -->",text)
new_text = new_text.replace("REPLACE_BY_DIR",param['site_dir'])
new_text = new_text.replace("REPLACE_BY_PATH",site_path)
new_text = r"""<?php session_start() ?>
<?php /* inclure au début de chaque page à protéger*/ include 'pass.php';?>
""" + new_text
html_file = open(os.path.join(param['dest_folder'],'index.php'), 'w')
html_file.write(new_text)
html_file.close()
print 'Fichier index.php écrit.'

# %% date in config.json
json_file = open(os.path.join(param['template_folder'],'config.json'), 'r')
json_text = json_file.read()
json_data = json.loads(json_text) # data is a list of dictionaries (keys = 'color, 'data', 'name')
json_file.close()
json_data['pubDate'] = unicode(time.strftime('%d/%m/%y',time.localtime()))
with open(param['dest_folder'] + '/config.json', 'w') as outfile:
    json.dump(json_data, outfile)
print 'Fichier config.json écrit.'

#%% Pass file
pass_file = open(os.path.join(templateFolder,'pass.php'), 'r')
pass_text = pass_file.read()
pass_file.close()
tipeur_passwd = raw_input('Mot de passe pour les tipeurs : ')
new_text = pass_text.replace("PASSWORD_TO_REPLACE",tipeur_passwd)
pass_file = open(os.path.join(destFolder,'pass.php'), 'w')
pass_file.write(new_text)
pass_file.close()
print 'Fichier pass.php écrit.'

print '********* Prêt pour export **********'

# %%  ENVOI DES FICHIERS
host = "ftp1.w4a.fr"
user = raw_input("Utilisateur FTP : ")
password = raw_input("Mot de passe : ")
ftp = FTP(host, user, password)

print "Connexion à " +host
etat = ftp.getwelcome()
print "Etat : ",etat


# Envoi des images
directory = '/var/www/pellichi.fr/htdocs' + site_path + '/imgs/pages'
ftp.cwd(directory) # changes directory
#ftp.retrlines('LIST') # list directory contents

os.chdir(destFolder_pages)
for im in pages_to_send:
#    path_to_file = os.path.join(destFolder_pages, im)
#    path_to_file = im #os.path.realpath(path_to_file)
    file_to_send = open(im,'rb')
    ftp.storbinary('STOR '+ im, file_to_send)# Envoi Fichier
    file_to_send.close()
    print "Image %s transférée." % im

os.chdir("..")
 
# Envoi de pages.json   
directory = '/var/www/pellichi.fr/htdocs' + site_path + '/data'
ftp.cwd(directory) # changes directory
filename = "pages.json"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename

# Envoi de index.html et config.json
directory = '/var/www/pellichi.fr/htdocs' + site_path
ftp.cwd(directory) # changes directory
filename = "config.json"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename
filename = "index.php"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename
filename = "pass.php"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename

ftp.close()
print "********** Tout s'est bien terminé ! **********"