# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 12:24:47 2017

@author: Globulle


Outil pour convertir tous les fichiers krita contenus dans les dossiers donnés en argument.

Utilisation :

    $ python kra2png.py dossier1 dossier2 dossier3
    
"""

import sys
import os

def main(argv):
   folders = argv
   if len(folders)==0:
       folders = [raw_input('Dossier à traiter : ')]
   for folder in folders:
       files = os.listdir(folder)
       krafiles = [x for x in files if x.endswith('.kra')] # only krita files
       for kra in krafiles:
            newfile = kra[:-3]+'png'
            os.system('krita %s --export --export-filename %s' %
            (os.path.join(folder,kra), os.path.join(folder,newfile)))
            print 'Fichier', newfile, 'exporté dans', folder


if __name__ == "__main__":
   main(sys.argv[1:])
   print '******* Terminé ! ********'
   
