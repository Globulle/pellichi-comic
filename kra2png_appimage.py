# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 12:24:47 2017

@author: Globulle


Outil pour convertir tous les fichiers krita contenus dans les dossiers donnés 
en argument en utilisant l'appimage krita contenue dans le même répertoire.

Utilisation :

    $ python kra2png_appimage.py dossier1 dossier2 dossier3
    
"""

import sys
import os

def main(argv):
   folders = argv
   if len(folders)==0:
       folders = [raw_input('Dossier à traiter : ')]
   for folder in folders:
       files = os.listdir(folder)
       krafiles = [x for x in files if x.endswith('.kra')] # only krita files
       for kra in krafiles:
            source_path = os.path.abspath(os.path.join(folder,kra))
            newfile = kra[:-3]+'png'
            dest_path = os.path.abspath(os.path.join(folder,newfile))
            os.system('./krita-3.0.1.1-x86_64.appimage %s --export --export-filename %s' % (source_path, dest_path)) # ne fonctionne pas avec krita-3.1.2-x86_64
            print 'Fichier', newfile, 'exporté dans', folder


if __name__ == "__main__":
   main(sys.argv[1:])
   print '******* Terminé ! ********'
   
