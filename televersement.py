# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 15:47:25 2017

@author: Globulle

------------------------------------

Script pour convertir les images .png en .jpg et les exporter sur le serveur.

Requiert 2 fichiers "template" : config.json et index.html

Les fichiers images d'entrée doivent être organisés comme suit :
 - Pages
     - begin
         - image de couverture
         - eventuelles autres pages de préambule
     - chap_00
         - 01.png
         - 02.png
         - ...
     - chap_01
         - 01.png
         - 02.png
         - ...
     - ...
     - end
         - image de conclusion
         
Il est possible d'ajoindre des notes à certaines pages : celles-ci doivent être
rédigées dans un fichier de même nom que l'image afférente (ex : 02.txt).

Pour reconvertir et ré-exporter l'ensemble des images, supprimer le dossier TO_EXPORT.
Par défaut, les images déjà présentes ne sont pas retraitées.

"""

import os
import json
import time
from ftplib import FTP   
import yaml
from PyPDF2 import PdfFileWriter, PdfFileReader

# %%
#site_directory = "mat_met"
#site_path = "/manga/" + param['site_dir']
#pageFolder = 'Pages'
#templateFolder = 'Templates'
#destFolder = 'TO_EXPORT' # A effacer pour relancer l'ensemble de l'export

def read_parameters(filename):
    param = yaml.load(open(filename))
    return param


def list_png(page_folder,lstFolders): # list of PNG files contained in subfolders
    lstPNG = []
    for folder in lstFolders:
        path = os.path.join(page_folder,folder)
        files = os.listdir(path)
        pngfiles = [x for x in files if x.endswith('.png')] # only png files
        pngfiles.sort()
        lstPNG.append(pngfiles)
#        print folder# pngfiles
    return lstPNG
    
def create_HQ_jpg(lst_transfo,param):
    if not os.path.isdir(param['dirHDjpg']): # creation of destination folder
        os.mkdir(param['dirHDjpg'])
    for item in lst_transfo:
        jpg_name = item[3] + '.jpg'
        dest_path = os.path.join(param['dirHDjpg'],jpg_name)
        os.system('convert %s -quality 80%% %s' % (item[0],dest_path))
        # Metadata : Then put credits
        write_metadata(dest_path,param,item[3])
        print "Fichier %s produit." % dest_path
        
# Creation of pdf from .jpg files
def create_pdf(lst_transfo,param,bookmarks): # TODO : ajouter onglets pour chapitres
    PDFname = param['ExportGenericName'] 
    files=""
    for item in lst_transfo:
        files += " " + param['dirHDjpg']+ '/' + item[3]+ '.jpg'
#    print 'convert %s %s' % (files,PDFname)
    os.system('convert %s %s' % (files,'temp.pdf')) 
    
    # Adding metadata
    cmd = 'exiftool '\
		 ' -Title="' + param['title'] + \
		 '" -Author="' + param['creator'] +  \
	 	 '" -overwrite_original -q temp.pdf'# + PDFname
    os.system(cmd)
    
    # Adding bookmarks
    PDFoutput = PdfFileWriter() # open output
    PDFinput = PdfFileReader('temp.pdf') # open input
    PDFoutput.appendPagesFromReader(PDFinput)
    PDFoutput.addBookmark('Page de garde', 0) # add bookmark
    chap_nb = 0
    for p in bookmarks[0:-2]:
        PDFoutput.addBookmark('Experience %d' % chap_nb, p) # add bookmark
        chap_nb +=1
    PDFoutputStream = file(PDFname,'wb') #creating result pdf 
    PDFoutput.write(PDFoutputStream) #writing to result pdf JCT
    PDFoutputStream.close() #closing result JCT   
    os.remove('temp.pdf')
    print "Fichier %s produit." % PDFname
    
def convert_ebook(param):
    os.system('zip %s.cbz %s/*.jpg' % (param['ExportGenericName'], param['dirHDjpg'])) # Create .cbz file
    print "Fichier %s.cbz produit." % param['ExportGenericName']
    cmd = 'ebook-convert ' + param['ExportGenericName'] +'.cbz ' + \
    param['ExportGenericName'] + '.epub' + \
    ' --authors "' + param['creator'] + \
    '" --publisher "' + param['creator'] + \
    '" --title "' + param['title'] + \
    '" --isbn "' + param['ebookIsbn'] + \
    '" --pubdate "' + time.strftime('%d/%m/%Y',time.localtime()) + \
    '" --language "' + param['language'] + \
    '" --tags "' + param['tags'] + \
    '" --comments "' + param['description'] + \
    '" --no-default-epub-cover --dont-normalize --keep-aspect-ratio' + \
    ' --output-profile tablet --no-process --disable-trim' + \
    ' --dont-add-comic-pages-to-toc --wide --extra-css' + \
    ' "img{width:100%}"' + \
    ' --cover "' + param['dirHDjpg'] + '/begin-page-1.jpg" --no-svg-cover --remove-first-image' # TODO : nom de fichier en variable au lieu de 'begin-page-1.jpg'
    os.system(cmd.encode('utf-8'))
    print "Fichier %s.epub produit." % param['ExportGenericName']

    # TODO : gestion des metadonnées


def create_json_page_entry(page_nb):             # %% Page entry in json file
    hotspot_text = unicode('top:0.00%;left:0.00%;width:100.00%;height:100.00%;') # unicode chain
    hotspot_entry = [hotspot_text] # list
    pagetext_entry = [] #list (possibility to add the dialogs)
    if not (folder=='begin' or folder=='end'):
        scripttext_entry = unicode('Chap.%d - p.%d' % (f,page_nb))
    elif folder=='begin':
        scripttext_entry = unicode('Ouverture - p.%d' % page_nb)
    else:
        scripttext_entry = unicode('Cloture - p.%d' % page_nb)
            
    json_page_entry = {unicode('hotspots'): hotspot_entry, unicode('number'): page_nb,
                       unicode('page_text'): pagetext_entry, unicode('script_text'): scripttext_entry}
    return json_page_entry


def write_metadata(image_name,param,page_name,web=False):
    if web:
        os.system('exiftool -all= -overwrite_original -q %s' % image_name) # Metadata : remove everything for privacy /!\ it also remove image dimensions!
    year = time.strftime('%Y',time.localtime())
    copyright_mention = "Copyright (c) " + param['creator'] + " " + year
    cmd = 'exiftool '\
    ' -EXIF:XPTitle="' + page_name + \
    '" -IPTC:ObjectName="'+ page_name + \
    '" -XMP-dc:Title="'+ page_name + \
    '" -XMP-xmpDM:ShotName="'+ page_name + \
    '" -EXIF:XPAuthor="' + param['creator'] + \
    '" -IPTC:By-line="' + param['creator'] + \
    '" -XMP-dc:Creator="' + param['creator'] + \
    '" -XMP-pdf:Author="' + param['creator'] + \
    '" -EXIF:Artist="' + param['creator'] + \
    '" -EXIF:Copyright="' + copyright_mention + \
    '" -EXIF:OwnerName="' + param['creator'] + \
    '" -EXIF:Usercomment="' + param['licence'] + \
    '" -EXIF:XPAuthor="' + param['creator'] + \
    '" -EXIF:XPComment="' + copyright_mention + \
    '" -IPTC:By-line="' + param['creator'] + \
    '" -IPTC:Contact="' + param['url'] + \
    '" -IPTC:CopyrightNotice="' + param['licence'] + \
    '" -IPTC:Credit="' + param['creator'] + \
    '" -Photoshop:CopyrightFlag="True' + \
    '" -Photoshop:URL="' + param['url'] + \
    '" -XMP-aux:OwnerName="' + param['creator'] + \
    '" -XMP-dc:Source="' + param['creator'] + \
    '" -XMP-dc:Creator="' + param['creator'] + \
    '" -XMP-dc:Rights="' + param['licence'] + \
    '" -XMP-pdf:Author="' + param['creator'] + \
    '" -XMP-photoshop:Credit="' + param['creator'] + \
    '" -XMP-photoshop:Source="' + param['creator'] + \
    '" -XMP-xmpDM:Copyright="' + copyright_mention + \
    '" -XMP-xmpRights:Marked="True' + \
    '" -XMP-xmpRights:Owner="' + param['creator'] + \
    '" -XMP-xmpRights:UsageTerms="' + param['licence'] + \
    '" -XMP-xmpRights:WebStatement="' + param['url'] + \
    '" -overwrite_original -q ' + image_name 
    os.system(cmd)


# %% Getting parameters
param = read_parameters('parameters.yaml')
site_path = "/" + param['site_metadir'] + "/" + param['site_dir']


# %% Listing files
lstFolders = os.listdir(param['page_folder'])
lstFolders.sort()


lstPNG = list_png(param['page_folder'],lstFolders) # list of PNG files

destFolder_pages = os.path.join(param['dest_folder'],'pages')

if not os.path.isdir(param['dest_folder']): # creation of destination folders
    os.mkdir(param['dest_folder'])
if not os.path.isdir(destFolder_pages):
    os.mkdir(destFolder_pages)
    

# %% Lists files and future page numbers
page_nb=0
bookmarks = [] # page numbers for the beginning of chapters
lst_transfo = [] # list of transformations

for f,folder in enumerate(lstFolders):  # loop on folders
    for page in lstPNG[f]:              # loop on pages within a folder
        page_nb+=1
        source_path = os.path.join(param['page_folder'],folder,page)
        image_title = '%s-page-%d' % (folder, page_nb)
        lst_transfo.append([source_path, folder, page_nb, image_title])
            
    if (page_nb % 2 == 1) and len(lstPNG[f])>0: #odd test: Add a white page to have chapter begin on right (even) side
        page_nb+=1
        image_title = '%s-page-%d' % (folder, page_nb)
        lst_transfo.append(["blank.png", folder, page_nb, image_title])
       
    bookmarks.append(page_nb+1)

os.system('convert %s  -fill white -colorize 100%% %s' % (lst_transfo[0][0], "blank.png")) # create a white page to insert when needed


# %% Creation of files needed for 'pulp' web viewer
json_page_entries = []
i_note = 0 # note counter
json_endnote_entries = []
pages_to_send = []

for item in lst_transfo:
    page_nb = item[2]
    new_page_name = 'page-%d.jpg' % page_nb
    dest_path = os.path.join(destFolder_pages,new_page_name)
    if os.path.isfile(dest_path):
        print 'Fichier %s déjà existant.' % dest_path
    else:
        os.system('convert -resize 75%% -quality 60%% -unsharp 0.48x0.48+0.50+0.012 %s  %s' % (item[0], dest_path)) # reduce size and quality
        write_metadata(dest_path,param,item[3],True)            
        print 'Fichier %s écrit.' % dest_path
        pages_to_send.append(new_page_name)
        
    # %% Page entry in json file
    json_page_entry = create_json_page_entry(page_nb)
    json_page_entries.append(json_page_entry)
    
    # %% Endnote entry in json file
    note_path = item[0][:-3] + 'txt'
    if os.path.exists(note_path):
        i_note += 1
        note_file = open(note_path,'r')
        text_entry = unicode(note_file.read())
        note_file.close()
        # url_entry = # useless since note text accepts html code
        pagepanel_entry = unicode('p.%d' % page_nb)
        json_note_entry = {unicode('number'):i_note, unicode('page_panel'): page_nb,
                           unicode('text'): text_entry}
        json_endnote_entries.append(json_note_entry)
        print "Note ajoutée."     
    
json_content = {unicode('endnotes'):json_endnote_entries, unicode('pages'):json_page_entries}

with open(param['dest_folder'] + '/pages.json', 'w') as outfile:
    json.dump(json_content, outfile)
print 'Fichier pages.json écrit.'

# %% Chapter index in index.html
html_file = open(os.path.join(param['template_folder'],'index.html'), 'r')
html_text = html_file.read()
html_file.close()
text = r"""<a href=""><li>Retour début</li></a>
    """
chap_nb = 0
for p in bookmarks[0:-2]:
    text = text + (r"""<a href="#%d"><li>Chapitre %d</li></a>
    """ % (p,chap_nb))
    chap_nb +=1
text = text + (r"""<a href="#%d"><li>...</li></a>""" % bookmarks[-2])

new_text = html_text.replace("<!-- LIST_OF_CHAPTERS -->",text)
new_text = new_text.replace("REPLACE_BY_DIR",param['site_dir'])
new_text = new_text.replace("REPLACE_BY_PATH",site_path)
html_file = open(os.path.join(param['dest_folder'],'index.html'), 'w')
html_file.write(new_text)
html_file.close()
print 'Fichier index.html écrit.'

# %% date in config.json
json_file = open(os.path.join(param['template_folder'],'config.json'), 'r')
json_text = json_file.read()
json_data = json.loads(json_text) # data is a list of dictionaries (keys = 'color, 'data', 'name')
json_file.close()
json_data['pubDate'] = unicode(time.strftime('%d/%m/%y',time.localtime()))
with open(param['dest_folder'] + '/config.json', 'w') as outfile:
    json.dump(json_data, outfile)
print 'Fichier config.json écrit.'

print '********* Prêt pour export **********'

# %%  ENVOI DES FICHIERS
host = param['ftp_host']
user = raw_input("Utilisateur FTP : ")
password = raw_input("Mot de passe : ")
ftp = FTP(host, user, password)

print "Connexion à " +host
etat = ftp.getwelcome()
print "Etat : ",etat


# Envoi des images
directory = '/var/www/pellichi.fr/htdocs' + site_path + '/imgs/pages'
ftp.cwd(directory) # changes directory
#ftp.retrlines('LIST') # list directory contents

os.chdir(destFolder_pages)
for im in pages_to_send:
#    path_to_file = os.path.join(destFolder_pages, im)
#    path_to_file = im #os.path.realpath(path_to_file)
    file_to_send = open(im,'rb')
    ftp.storbinary('STOR '+ im, file_to_send)# Envoi Fichier
    file_to_send.close()
    print "Image %s transférée." % im

os.chdir("..")
 
# Envoi de pages.json   
directory = '/var/www/pellichi.fr/htdocs' + site_path + '/data'
ftp.cwd(directory) # changes directory
filename = "pages.json"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename

# Envoi de index.html et config.json
directory = '/var/www/pellichi.fr/htdocs' + site_path
ftp.cwd(directory) # changes directory
filename = "config.json"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename
filename = "index.html"
file_to_send = open(filename,'rb')
ftp.storbinary('STOR '+ filename, file_to_send)# Envoi Fichier
file_to_send.close()
print "Fichier %s transféré." % filename

ftp.close()
print "********** Tout s'est bien terminé ! **********"